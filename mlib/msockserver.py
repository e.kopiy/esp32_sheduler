import streams
import socket
import urlparse
import os
import gc
import timers
import mcu
import json

new_resource("res/home.shtml")

class Msockserver():

    mutils_inst = None

    cgi_avail_path = (
        "/reboot",
        "/clearsettings",
        "/applysettings"
    )
    
    html_avail_path = (
        "/home.shtml"
    )
    
    port = 80
    sock = 0
    conncnt = 0
    
    def __init__(self, mutils_inst):
        self.mutils_inst = mutils_inst

    def parse_rinfo(self, rinfo):
        ret = {
            "path":"",
            "qparams":{}
        }
        if rinfo != '':
            ritems = rinfo.split(' ')
            if len(ritems) > 2:
                if ritems[0] == 'GET':
                    uri = str(ritems[1]).strip()
                    # parse uri
                    u = urlparse.parse(uri)
                    path = str(u[2])
                    qparams = urlparse.parse_qs(str(u[3]))
                    if path == "/":
                        path = "/home.shtml"
                    """
                    ret["path"] = str(u[2])
                    ret["qparams"] = urlparse.parse_qs(str(u[3]))
                    if ret["path"] == "":
                        ret["path"] = "home.shtml"
                    """
                    u = None
            ritems = None
            
        return (uri, path, qparams)
                        
    def apply_substit(self, src_st, subst_data):
        if len(subst_data) == 0:
            print("apply_substit: Empty subst_data")
            return src_st

        ret = src_st
        if "{{ " in src_st:
            fromp = 0
            scnt = 0
            p1 = src_st.find("{{ ", fromp)
            while p1 >= 0:
                p2 = src_st.find(" }}", fromp) + 3
                fromp = p2
                v = str(src_st[p1:p2])
                sind = v.lower()[3:-3]
                if sind in subst_data:
                    ret = src_st.replace(v, str(subst_data[sind]))
                p1 = src_st.find("{{ ", fromp)
                scnt += 1
                if scnt > 10:
                    print("Max substitution in one line: 10. Skiping..")
                    break
        fromp = None
        scnt = None
        p1 = None
        p2 = None
        v = None
        sind = None
        
        ret = ret.replace("-",":")
        return ret
        
    def static_from_res(self, path, clientsock, subtdata = {}):
        print("Try to send static with path",path," with subst:",subtdata)
        ret = False
        try:
            res_name = str(path[1:])
            print("Try to open res:",res_name)
            ff = open("resource://"+res_name)
            print("Resource opened")
            ret = ""
            while True:
                line = ff.readline()
                if not line:
                    break
                if len(subtdata)>0:
                    line = self.apply_substit(line, subtdata)
                if len(line) > 1024:
                    clientsock.sendall(ret)
                    ret = ""
                else:
                    ret += line
            if ret != "":
                clientsock.sendall(ret)
            
            line = None
            ret = None
        except Exception as e:
            print("Muserhttph: get_static: resource://"+res_name+" not found", e)
            
                        
    def send_headers(self, hcode, hdescr, clientsock):
        clientsock.sendall("HTTP/1.1 " + str(hcode) + " " + hdescr + "\r\n")
        clientsock.sendall("Content-Type: text/html\r\n")
        clientsock.sendall("Connection: close\r\n\r\n")

    def get_sts_data(self, ord):
        result = ""
        
        if ord == 1:
            result = "[" + "\"" + self.mutils_inst.stgs.settings["shedule"]["1st_mon"] + "\", \"" + self.mutils_inst.stgs.settings["shedule"]["1st_tue"] + "\", \"" + self.mutils_inst.stgs.settings["shedule"]["1st_wed"] + "\", \"" + self.mutils_inst.stgs.settings["shedule"]["1st_thu"] + "\", \"" + self.mutils_inst.stgs.settings["shedule"]["1st_fri"] + "\", \"" + self.mutils_inst.stgs.settings["shedule"]["1st_sat"] + "\", \"" + self.mutils_inst.stgs.settings["shedule"]["1st_sun"] + "\"" + "]"
        if ord == 2:
            result = "[" + "\"" + self.mutils_inst.stgs.settings["shedule"]["2nd_mon"] + "\", \"" + self.mutils_inst.stgs.settings["shedule"]["2nd_tue"] + "\", \"" + self.mutils_inst.stgs.settings["shedule"]["2nd_wed"] + "\", \"" + self.mutils_inst.stgs.settings["shedule"]["2nd_thu"] + "\", \"" + self.mutils_inst.stgs.settings["shedule"]["2nd_fri"] + "\", \"" + self.mutils_inst.stgs.settings["shedule"]["2nd_sat"] + "\", \"" + self.mutils_inst.stgs.settings["shedule"]["2nd_sun"] + "\"" + "]"
        
        result = result.replace("false", "0")
        result = result.replace("true", "1")
        
        return result

    def get_main_page_substits(self):

        if "fwinfo" not in self.mutils_inst.stgs.settings:
            self.mutils_inst.stgs.settings["fwinfo"] = {}
        
        if "apn" not in self.mutils_inst.stgs.settings["wifi"]:
            self.mutils_inst.stgs.settings["wifi"]["apn"] = ""
        if self.mutils_inst.stgs.settings["wifi"]["apn"] == "":
            self.mutils_inst.stgs.settings["wifi"]["apn"] = "DbgAP_" + self.mutils_inst.mcuuid[-6:]
            
        if "app" not in self.mutils_inst.stgs.settings["wifi"]:
            self.mutils_inst.stgs.settings["wifi"]["app"] = ""
        if self.mutils_inst.stgs.settings["wifi"]["app"] == "":
            self.mutils_inst.stgs.settings["wifi"]["app"] = "111222333444555"


        if "shedule" not in self.mutils_inst.stgs.settings:
            self.mutils_inst.stgs.settings["shedule"] = {}

        if "1st_time_open_from" not in self.mutils_inst.stgs.settings["shedule"]:
            self.mutils_inst.stgs.settings["shedule"]["1st_time_open_from"] = "00-00"
        if "1st_time_open_to" not in self.mutils_inst.stgs.settings["shedule"]:
            self.mutils_inst.stgs.settings["shedule"]["1st_time_open_to"] = "23-59"
        
        if "1st_mon" not in self.mutils_inst.stgs.settings["shedule"]:
            self.mutils_inst.stgs.settings["shedule"]["1st_mon"] = "false"
        if "1st_tue" not in self.mutils_inst.stgs.settings["shedule"]:
            self.mutils_inst.stgs.settings["shedule"]["1st_tue"] = "false"
        if "1st_wed" not in self.mutils_inst.stgs.settings["shedule"]:
            self.mutils_inst.stgs.settings["shedule"]["1st_wed"] = "false"
        if "1st_thu" not in self.mutils_inst.stgs.settings["shedule"]:
            self.mutils_inst.stgs.settings["shedule"]["1st_thu"] = "false"
        if "1st_fri" not in self.mutils_inst.stgs.settings["shedule"]:
            self.mutils_inst.stgs.settings["shedule"]["1st_fri"] = "false"
        if "1st_sat" not in self.mutils_inst.stgs.settings["shedule"]:
            self.mutils_inst.stgs.settings["shedule"]["1st_sat"] = "false"
        if "1st_sun" not in self.mutils_inst.stgs.settings["shedule"]:
            self.mutils_inst.stgs.settings["shedule"]["1st_sun"] = "false"


        if "2nd_time_open_from" not in self.mutils_inst.stgs.settings["shedule"]:
            self.mutils_inst.stgs.settings["shedule"]["2nd_time_open_from"] = "00-00"
        if "2nd_time_open_to" not in self.mutils_inst.stgs.settings["shedule"]:
            self.mutils_inst.stgs.settings["shedule"]["2nd_time_open_to"] = "23-59"
        
        if "2nd_mon" not in self.mutils_inst.stgs.settings["shedule"]:
            self.mutils_inst.stgs.settings["shedule"]["2nd_mon"] = "false"
        if "2nd_tue" not in self.mutils_inst.stgs.settings["shedule"]:
            self.mutils_inst.stgs.settings["shedule"]["2nd_tue"] = "false"
        if "2nd_wed" not in self.mutils_inst.stgs.settings["shedule"]:
            self.mutils_inst.stgs.settings["shedule"]["2nd_wed"] = "false"
        if "2nd_thu" not in self.mutils_inst.stgs.settings["shedule"]:
            self.mutils_inst.stgs.settings["shedule"]["2nd_thu"] = "false"
        if "2nd_fri" not in self.mutils_inst.stgs.settings["shedule"]:
            self.mutils_inst.stgs.settings["shedule"]["2nd_fri"] = "false"
        if "2nd_sat" not in self.mutils_inst.stgs.settings["shedule"]:
            self.mutils_inst.stgs.settings["shedule"]["2nd_sat"] = "false"
        if "2nd_sun" not in self.mutils_inst.stgs.settings["shedule"]:
            self.mutils_inst.stgs.settings["shedule"]["2nd_sun"] = "false"

        sts1_data = self.get_sts_data(1)
        sts2_data = self.get_sts_data(2)

        sdata = {
            # Wifi info
            "apname":self.mutils_inst.stgs.settings["wifi"]["apn"] if "apn" in self.mutils_inst.stgs.settings["wifi"] else "",
            "appassw":self.mutils_inst.stgs.settings["wifi"]["app"] if "app" in self.mutils_inst.stgs.settings["wifi"] else "",
            "staname":self.mutils_inst.stgs.settings["wifi"]["stn"] if "stn" in self.mutils_inst.stgs.settings["wifi"] else "",
            "stapassw":self.mutils_inst.stgs.settings["wifi"]["stp"] if "stp" in self.mutils_inst.stgs.settings["wifi"] else "",
            # 1st_shedule time
            "1st_time_open_from":self.mutils_inst.stgs.settings["shedule"]["1st_time_open_from"] if "1st_time_open_from" in self.mutils_inst.stgs.settings["shedule"] else "",
            "1st_time_open_to":self.mutils_inst.stgs.settings["shedule"]["1st_time_open_to"] if "1st_time_open_to" in self.mutils_inst.stgs.settings["shedule"] else "",
            # 1st_shedule days
            "1sts_data":sts1_data,
            
            # 2nd_shedule time
            "2nd_time_open_from":self.mutils_inst.stgs.settings["shedule"]["2nd_time_open_from"] if "2nd_time_open_from" in self.mutils_inst.stgs.settings["shedule"] else "",
            "2nd_time_open_to":self.mutils_inst.stgs.settings["shedule"]["2nd_time_open_to"] if "2nd_time_open_to" in self.mutils_inst.stgs.settings["shedule"] else "",
            # 2nd_shedule days
            "2sts_data":sts2_data
        }
        
        sts1_data = None
        sts2_data = None

        return sdata
    
    def cgi_apply_settings(self, qparams):

        if self.mutils_inst.ap_state == False:
            self.mutils_inst.shed_inst.stop_sheduler()
            self.mutils_inst.stop_print_free_mem_timer()
        else:
            self.mutils_inst.mwifi_inst.stop_ap_handler()

        # Wifi settings
        if "wifi" not in self.mutils_inst.stgs.settings:
            self.mutils_inst.stgs.settings["wifi"] = {}
        if "apn" in qparams:
            self.mutils_inst.stgs.settings["wifi"]["apn"] = qparams["apn"]
        if "app" in qparams:
            self.mutils_inst.stgs.settings["wifi"]["app"] = qparams["app"]
        if "stn" in qparams:
            self.mutils_inst.stgs.settings["wifi"]["stn"] = qparams["stn"]
        if "stp" in qparams:
            self.mutils_inst.stgs.settings["wifi"]["stp"] = qparams["stp"]

        # Shedule settings
        if "shedule" not in self.mutils_inst.stgs.settings:
            self.mutils_inst.stgs.settings["shedule"] = {}
        if "1st__time_open_from" in qparams:
            self.mutils_inst.stgs.settings["shedule"]["1st_time_open_from"] = qparams["1st_time_open_from"]
        if "1st_time_open_to" in qparams:
            self.mutils_inst.stgs.settings["shedule"]["1st_time_open_to"] = qparams["1st_time_open_to"]

        if "1st_mon" in qparams:
            self.mutils_inst.stgs.settings["shedule"]["1st_mon"] = qparams["1st_mon"]
        if "1st_tue" in qparams:
            self.mutils_inst.stgs.settings["shedule"]["1st_tue"] = qparams["1st_tue"]
        if "1st_wed" in qparams:
            self.mutils_inst.stgs.settings["shedule"]["1st_wed"] = qparams["1st_wed"]
        if "1st_thu" in qparams:
            self.mutils_inst.stgs.settings["shedule"]["1st_thu"] = qparams["1st_thu"]
        if "1st_fri" in qparams:
            self.mutils_inst.stgs.settings["shedule"]["1st_fri"] = qparams["1st_fri"]
        if "1st_sat" in qparams:
            self.mutils_inst.stgs.settings["shedule"]["1st_sat"] = qparams["1st_sat"]
        if "1st_sun" in qparams:
            self.mutils_inst.stgs.settings["shedule"]["1st_sun"] = qparams["1st_sun"]


        if "2nd_time_open_from" in qparams:
            self.mutils_inst.stgs.settings["shedule"]["2nd_time_open_from"] = qparams["2nd_time_open_from"]
        if "2nd_time_open_to" in qparams:
            self.mutils_inst.stgs.settings["shedule"]["2nd_time_open_to"] = qparams["2nd_time_open_to"]

        if "2nd_mon" in qparams:
            self.mutils_inst.stgs.settings["shedule"]["2nd_mon"] = qparams["2nd_mon"]
        if "2nd_tue" in qparams:
            self.mutils_inst.stgs.settings["shedule"]["2nd_tue"] = qparams["2nd_tue"]
        if "2nd_wed" in qparams:
            self.mutils_inst.stgs.settings["shedule"]["2nd_wed"] = qparams["2nd_wed"]
        if "2nd_thu" in qparams:
            self.mutils_inst.stgs.settings["shedule"]["2nd_thu"] = qparams["2nd_thu"]
        if "2nd_fri" in qparams:
            self.mutils_inst.stgs.settings["shedule"]["2nd_fri"] = qparams["2nd_fri"]
        if "2nd_sat" in qparams:
            self.mutils_inst.stgs.settings["shedule"]["2nd_sat"] = qparams["2nd_sat"]
        if "2nd_sun" in qparams:
            self.mutils_inst.stgs.settings["shedule"]["2nd_sun"] = qparams["2nd_sun"]

        self.mutils_inst.stgs.write_settings()
        
        print("Settings saved:", self.mutils_inst.stgs.settings)
    
    def readsockline(self, clientsock):
        ret = ""
        buf = bytearray(1)
        nline = 1
        nbyte = 0
        maxbytes = 1024
        lb = "0000"
        while True:
            n = clientsock.recv_into(buf,1,0,0)
            if n == 0:
                break
            lb = lb[1:4] + buf
            if lb == "\r\n\r\n":
                break
            if buf == "\n":
                nline += 1
            nbyte += 1
            if nline == 1:
                ret += buf
            if nbyte>maxbytes:
                print("Max socket read bytes count:",nbyte," Exiting...")
        buf = None
        nline = None
        nbyte = None
        maxbytes = None
        lb = None
        
        return ret

    def poll_proc(self):
        while True:
            try:
                print("Waiting for connection...")
                # here we wait for a connection
                clientsock,addr = self.sock.accept()
                try:
                    print("Incoming connection from",addr, ", active connections: ",self.conncnt)
                    print("FreeMem5:", str(gc.info()[1]//1024))
                    if self.mutils_inst.ap_state == False:
                        self.mutils_inst.shed_inst.stop_sheduler()
                    if self.conncnt > 0:
                        print('Server busy. Reseting connection...')
                        clientsock.close()
                        clientsock = None
                    else:
                        self.conncnt += 1
                        ri = self.readsockline(clientsock);
                        #print("FreeMem6:", str(gc.info()[1]//1024))
                        print("Read line from socket:", ri)
                        if ri != "":
                            uri, path, qparams = self.parse_rinfo(ri)
                            
                            print("Requested path:", path)
                            #print("Parsed reqinfo:", uri, path, qparams)
                            if path in self.html_avail_path:
                                self.send_headers(200, "OK", clientsock)
                                #print("FreeMem8:", str(gc.info()[1]//1024))
                                #print("Headers was sent to client")
                                
                                if path == '/home.shtml':
                                    #print(self.mutils_inst.stgs.settings)
                                    subst = self.get_main_page_substits()
                                    #print("FreeMem9:", str(gc.info()[1]//1024))
                                    self.static_from_res(path, clientsock, subst)
                                    #print("FreeMem10:", str(gc.info()[1]//1024))
                                    subst = None
                                else:
                                    clientsock.sendall("No handler assigned for path:",path)
                            elif path in self.cgi_avail_path:
                                self.send_headers(200, "OK", clientsock)
                                # handle cgi and send contents here
                                if path == "/reboot":
                                    print("Reboot webreq catched. Rebooting unit...")
                                    clientsock.sendall("{}\n")
                                    clientsock.close()
                                    mcu.reset()
                                elif path == "/clearsettings":
                                    print("Reset settings webreq catched. Reseting...")
                                    self.mutils_inst.stgs.settings_init()
                                    clientsock.sendall("{}\n")
                                    qparams = None
                                elif path == "/applysettings":
                                    self.cgi_apply_settings(qparams)
                                    clientsock.sendall("{}\n")
                                    qparams = None
                                    clientsock.close()
                                    sleep(1000)
                                    mcu.reset()
                            else:
                                self.send_headers(404, "Not Found", clientsock)
                                clientsock.sendall("404 Not Found")
                                print("404 Not Found:",path)
                            uri = None
                            path = None
                            qparam = None
                            
                        ri = None
                        
                        #print("FreeMem11:", str(gc.info()[1]//1024))
                        print("Closing client sock")
                        clientsock.close()
                        sdata = {}
                        self.conncnt = 0
                    if self.mutils_inst.ap_state == False:
                        self.mutils_inst.shed_inst.run_sheduler()
                    print("FreeMem11:", str(gc.info()[1]//1024))
                except Exception as e:
                    self.conncnt = 0
                    print("Can not handle user req:",e)                    
            except Exception as e:
                self.conncnt = 0
                print("sock.accept exception:",e)
    
    def poll(self):
        thread(self.poll_proc,self)
        
    def load_from_resource(self, mresource):
        mstream = open(mresource)
        barray = bytearray()
        while True:
            rd = mstream.read(1)
            if not rd:
                barray.append(0x0)
                break
            barray.append(rd[0])
        return barray

    def run(self):
        self.sock = socket.socket()
        self.sock.bind(self.port)
        self.sock.listen()