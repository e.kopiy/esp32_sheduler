import mcu
import gc
import rtc
import socket
import timers

import json

from mlib import mwifi
from mlib import msettings
from mlib import msockserver
from mlib import msheduler

class MUtils():
    ap_state = False
    
    print_free_mem_timer = None

    wifi_enabled = True
    wait_inet = True
    sockserv_enabled = True

    mcuuid = ""
    stgs = None # settings
    mwifi_inst = None # wifi
    shed_inst = None # wifi
    mws_inst = None # sockserver
    cmdh = None  # tcp handler

    ssl_ctx = None

    settings = {}

    def __init__(self, ap_state):
        self.ap_state = ap_state
        try:
            self.mcuuid = self.get_uid_str()
        except Exception as e:
            print("MUtils() init exception > ", e)

    def get_uid_str(self):
        r = ""
        try:
            for ch in mcu.uid():
                hv = hex(ch).replace('0x', '')
                if len(hv) == 1:
                    hv = '0'+hv
                hv = hv[1] + hv[0]
                r = r + hv
        except Exception as e:
            print(e)
        return r
        
    def print_free_mem(self):
        freemem = gc.info()[1]//1024
        print("FreeMem000:", str(freemem))
        if freemem < 12:
            print("### WARNING! Not enough memory. Rebooting NOW!")
            mcu.reset()
        
            
    def load_from_resource(self, mresource):
        mstream = open(mresource)
        barray = bytearray()
        while True:
            rd = mstream.read(1)
            if not rd:
                barray.append(0x0)
                break
            barray.append(rd[0])
        return barray

    def start_print_free_mem_timer(self):
        try:
            self.print_free_mem_timer=timers.timer()
            self.print_free_mem_timer.interval(2500, self.print_free_mem)
            self.print_free_mem_timer.start()
        except Exception as e:
            print(" >", e)

    def run_print_free_mem_timer(self):
        try:
            self.print_free_mem_timer.interval(2500, self.print_free_mem)
            self.print_free_mem_timer.start()
        except Exception as e:
            print(" >", e)

    def stop_print_free_mem_timer(self):
        try:
            self.print_free_mem_timer.clear()
        except Exception as e:
            print(" >", e)

    def run_utils(self):
        print("FreeMem1:", str(gc.info()[1]//1024))
        
        self.stgs = msettings.Msettings()
        self.stgs.read_settings()
        print("Loaded settings:", self.stgs.settings)
        
        print("FreeMem2:", str(gc.info()[1]//1024))        
        
        if self.wifi_enabled:
            print("Setup wifi")
            try:
                self.mwifi_inst = mwifi.Mwifi(self)
                self.start_print_free_mem_timer()
                if self.ap_state:
                    self.mwifi_inst.start_ap_handler()
                else:
                    self.mwifi_inst.start_conn_handler()
                    print("### SHED: Trying to create scheduler")
                    self.shed_inst = msheduler.MSheduler(self)
                    print("### SHED: Create instance OK. Trying to run sheduler")
                    if self.shed_inst.get_shedule_from_settings() == True:
                        self.shed_inst.start_sheduler()
                    else:
                        print("### SHED_WARNING: Can't load shedule data")
            except Exception as e:
                print("Mwifi() exception: ", e)
                
            print("Setup wifi complete")
            
            print("Starting tcp server")
            self.mws_inst = msockserver.Msockserver(self)
            self.mws_inst.run()
            self.mws_inst.poll()
            print("FreeMem4:", str(gc.info()[1]//1024))