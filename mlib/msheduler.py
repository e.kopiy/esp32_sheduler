import socket
import streams
import requests
import gc
import timers
from wireless import wifi
from maxim.ds1307 import ds1307

class MSheduler():

    mutils_inst = None
    ds = None
    date_time_is_now = ""
    
    first_hr_open_from = 0
    first_hr_open_to = 0
    first_min_open_from = 0
    first_min_open_to = 0
    first_open_from = 0
    first_open_to = 0
    
    sec_hr_open_from = 0
    sec_hr_open_to = 0
    sec_min_open_from = 0
    sec_min_open_to = 0
    sec_open_from = 0
    sec_open_to = 0    
    first_weekdays = []
    
    second_open_from = ""
    second_open_to = ""
    second_weekdays = []
    
    shedule = []
    
    last_time_sync = 0
    time_to_sync = 0

    shedule_handler_timer = None

    def __init__(self, mutils_inst):
        try:
            self.mutils_inst = mutils_inst
            
            print("### SHED_INFO: Trying to init RTC")
            
            self.ds = ds1307.DS1307(I2C2)
            self.ds.start()
        except Exception as e:
            print("### SHED_ERROR: MSheduler() init exception")

    def is_leap_year(self, y):
        if y % 400 == 0:
            return True
        if y % 100 == 0:
            return False
        if y % 4 == 0:
            return True
        else:
            return False
    
    def ntc_ts_to_datetime(self, ntp_time):
        # convert NTP timestamp (seconds since January 1st 1900) to datetime formatted string
        # YYYY-mm-dd HH:MM:SS
        result = 0
        t = [ 0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4 ]
        ts = ntp_time - 2208988800
    
        s = ts % 60
        ts //= 60
        m = ts % 60
        ts //= 60
        h = ts % 24
        ts //= 24
    
        a = (4 * ts + 102032) // 146097 + 15
        b = (ts + 2442113 + a - (a//4))
        c = (20 * b - 2442) // 7305
        d =  b - 365* c - (c // 4)
        e = d * 1000 //30601
        f = d - e*30 - e*601//1000
    
        if e <= 13:
            c-=4716
            e -= 1
        else:
            c -= 4715
            e -= 13
    
        Y = c
        M = e
        D = f

        Y -= M < 3
        day_of_week = (Y + int(Y / 4) - int(Y / 100) + int(Y / 400) + t[M - 1] + D) % 7
        
        if self.is_leap_year(Y):
            if M == 2 and D == 29:
                day_of_week += 1
            if M > 2:
                day_of_week += 1
        result = [h, m, s, D, M, Y, day_of_week]
        return result
        
    def ready_to_sync(self):
        if self.last_time_sync == 0:
            self.last_time_sync = self.shedule_handler_timer.get()
            self.time_to_sync = 5400000 + random(0, 60000) + self.last_time_sync
            return True
        if self.last_time_sync != 0:
            if (self.shedule_handler_timer.get() - self.time_to_sync) > 0:
                self.last_time_sync = self.shedule_handler_timer.get()
                self.time_to_sync = 5400000 + random(0, 60000) + self.last_time_sync
                return True
            else:
                return False

    def sync_time(self):
        try:
            if (self.mutils_inst.mwifi_inst.net_config_is_ok()):
                its_sync_time = self.ready_to_sync()
                if its_sync_time == True:
                    self.shedule_handler_timer.clear()
                    self.mutils_inst.stop_print_free_mem_timer()
                    if self.mutils_inst.ap_state:
                        self.mutils_inst.mwifi_inst.stop_ap_handler()
                    else:
                        self.mutils_inst.mwifi_inst.stop_conn_handler()
                    print("### INFO: Timers stopped to sync time")
                        
                    # create an UDP socket and set a timeout of 1 second
                    sock = socket.socket(type=socket.SOCK_DGRAM)
                    sock.settimeout(1000)
                
                    # create an NTP request packet
                    pkt = bytearray([0]*48)
                    pkt[0] = 0x1B        
                    try:
                        # resolve the NTP server hostname to get its IP address
                        ip_string = wifi.gethostbyname("0.pool.ntp.org")
                    except Exception as e:
                        print("### SHED_WARNING: unable to resolve NTP server domain. ", e)
                        return False
                    ip = socket.ip_to_tuple(ip_string)
            
                    # create a tuple containing NTP server ip address and port
                    addr = (ip[0], ip[1], ip[2], ip[3], 123)
            
                    print("### SHED_DEBUG: Sending NTP request to %s:%d" %(ip_string, 123))
                    # send the NTP request packet to the NTP server
                    sock.sendto(pkt, addr)
            
                    # read the response from the server
                    res = sock.recv(48)
            
                    # extract the "transmit timestamp" field from the received packet
                    ts =  (res[40]<<24) | (res[41]<<16) | (res[42]<<8) | res[43]
            
                    # set RTC time
                    print("### SHED_DEBUG: DATETIME FOR DS:", self.ntc_ts_to_datetime(ts+10800))
                    ds_time = (self.ntc_ts_to_datetime(ts+10800))
                    if ds_time != 0:
                        self.ds.set_time(ds_time[0],ds_time[1],ds_time[2],ds_time[3],ds_time[4],ds_time[5],ds_time[6])
                        print("### SHED_INFO: Time sync success")
                    else:
                        print("### SHED_DEBUG: ntc_ts_to_datetime returns zero")
                    self.shedule_handler_timer.interval(1000, self.shedule_handler)
                    self.shedule_handler_timer.start()
                    self.mutils_inst.run_print_free_mem_timer()
                    if self.mutils_inst.ap_state:
                        self.mutils_inst.mwifi_inst.run_ap_handler()
                    else:
                        self.mutils_inst.mwifi_inst.run_conn_handler()
                    print("### INFO: Timers started after sync time")
            else:
                print("### SHED_TIME_SYNC_ERROR: Network config is wrong. Skipping timesync.")
        except Exception as e:
            print("### INFO: Time sync error. Trying to rerun timers")
            self.shedule_handler_timer.interval(1000, self.shedule_handler)
            self.shedule_handler_timer.start()
            self.mutils_inst.run_print_free_mem_timer()
            if self.mutils_inst.ap_state:
                self.mutils_inst.mwifi_inst.run_ap_handler()
            else:
                self.mutils_inst.mwifi_inst.run_conn_handler()
            print("### ERROR: Cant sync time. Exception >", e)

    def get_date_time(self):
        try:
            return("%02d:%02d:%02d - %02d/%02d/%d - %d"%self.ds.get_time())
        except Exception as e:
            print(" >", e)

    def get_shedule_from_settings(self):
        
        try:
            self.first_weekdays.clear()
            self.second_weekdays.clear()
            
            result = []
            if "shedule" in self.mutils_inst.stgs.settings:
                if "1st_time_open_from" in self.mutils_inst.stgs.settings["shedule"]:
                    if self.mutils_inst.stgs.settings["shedule"]["1st_time_open_from"] != "":
                        if "1st_time_open_to" in self.mutils_inst.stgs.settings["shedule"]:
                            if self.mutils_inst.stgs.settings["shedule"]["1st_time_open_to"] != "":
                                self.first_hr_open_from = self.mutils_inst.stgs.settings["shedule"]["1st_time_open_from"][0:2]
                                self.first_min_open_from = self.mutils_inst.stgs.settings["shedule"]["1st_time_open_from"][len(self.mutils_inst.stgs.settings["shedule"]["1st_time_open_from"])-2:len(self.mutils_inst.stgs.settings["shedule"]["1st_time_open_from"])]
                                self.first_hr_open_to = self.mutils_inst.stgs.settings["shedule"]["1st_time_open_to"][0:2]
                                self.first_min_open_to = self.mutils_inst.stgs.settings["shedule"]["1st_time_open_to"][len(self.mutils_inst.stgs.settings["shedule"]["1st_time_open_to"])-2:len(self.mutils_inst.stgs.settings["shedule"]["1st_time_open_to"])]
                                self.first_weekdays = [self.mutils_inst.stgs.settings["shedule"]["1st_mon"],
                                                    self.mutils_inst.stgs.settings["shedule"]["1st_tue"],
                                                    self.mutils_inst.stgs.settings["shedule"]["1st_wed"],
                                                    self.mutils_inst.stgs.settings["shedule"]["1st_thu"],
                                                    self.mutils_inst.stgs.settings["shedule"]["1st_fri"],
                                                    self.mutils_inst.stgs.settings["shedule"]["1st_sat"],
                                                    self.mutils_inst.stgs.settings["shedule"]["1st_sun"]]
                                result.append("first")
                if "2nd_time_open_from" in self.mutils_inst.stgs.settings["shedule"]:
                    if self.mutils_inst.stgs.settings["shedule"]["2nd_time_open_from"] != "":
                        if "2nd_time_open_to" in self.mutils_inst.stgs.settings["shedule"]:
                            if self.mutils_inst.stgs.settings["shedule"]["2nd_time_open_to"] != "":
                                self.sec_hr_open_from = self.mutils_inst.stgs.settings["shedule"]["2nd_time_open_from"][0:2]
                                self.sec_min_open_from = self.mutils_inst.stgs.settings["shedule"]["2nd_time_open_from"][len(self.mutils_inst.stgs.settings["shedule"]["2nd_time_open_from"])-2:len(self.mutils_inst.stgs.settings["shedule"]["2nd_time_open_from"])]
                                self.sec_hr_open_to = self.mutils_inst.stgs.settings["shedule"]["2nd_time_open_to"][0:2]
                                self.sec_min_open_to = self.mutils_inst.stgs.settings["shedule"]["2nd_time_open_to"][len(self.mutils_inst.stgs.settings["shedule"]["2nd_time_open_to"])-2:len(self.mutils_inst.stgs.settings["shedule"]["2nd_time_open_to"])]
                                
                                self.second_weekdays = [str(self.mutils_inst.stgs.settings["shedule"]["2nd_mon"]),
                                                    str(self.mutils_inst.stgs.settings["shedule"]["2nd_tue"]),
                                                    str(self.mutils_inst.stgs.settings["shedule"]["2nd_wed"]),
                                                    str(self.mutils_inst.stgs.settings["shedule"]["2nd_thu"]),
                                                    str(self.mutils_inst.stgs.settings["shedule"]["2nd_fri"]),
                                                    str(self.mutils_inst.stgs.settings["shedule"]["2nd_sat"]),
                                                    str(self.mutils_inst.stgs.settings["shedule"]["2nd_sun"])]
                                result.append("second")
            if len(result) != 0:
                self.shedule = result
                self.first_open_from = int(self.first_hr_open_from)*60*60 + int(self.first_min_open_from)*60
                self.first_open_to = int(self.first_hr_open_to)*60*60 + int(self.first_min_open_to)*60
                self.sec_open_from = int(self.sec_hr_open_from)*60*60 + int(self.sec_min_open_from)*60
                self.sec_open_to = int(self.sec_hr_open_to)*60*60 + int(self.sec_min_open_to)*60
                print ("### SHED_INFO: Shedule loaded successful")
                return True
            else:
                print ("### SHED_ERROR: Can't load shedule from memory")
                return False
        except Exception as e:
            print("### FATAL_ERROR: Sheduler exception: ", e)

    def set_door_status(self):
        status = "closed"
        try:
            self.date_time_is_now = self.get_date_time()
            if self.date_time_is_now != False:
                time_now = int(self.date_time_is_now[0:2])*60*60 + int(self.date_time_is_now[3:5])*60
                day_of_week = int(self.date_time_is_now[len(self.date_time_is_now)-1:len(self.date_time_is_now)])
                for i in range(len(self.shedule)):
                    if (self.shedule[i] == "first") and (self.first_weekdays[day_of_week-1] == "true"):
                        if (time_now >= self.first_open_from) and (time_now <= self.first_open_to):
                            status = "opened"
                        else:
                            if status != "opened":
                                status = "closed"
                    else:
                        if status != "opened":
                            status = "closed"
                    if (self.shedule[i] == "second") and (self.second_weekdays[day_of_week-1] == "true"):
                        if (time_now >= self.sec_open_from) and (time_now <= self.sec_open_to):
                            status = "opened"
                        else:
                            if status != "opened":
                                status = "closed"
                    else:
                        if status != "opened":
                            status = "closed"

                if status == "opened":
                    print("Current time: ", self.date_time_is_now, ", DOOR_STATUS: OPENED")
                    digitalWrite(D5, LOW)
                if status == "closed":
                    print("Current time: ", self.date_time_is_now, ", DOOR STATUS: CLOSED")
                    digitalWrite(D5, HIGH)
            else:
                print("### ERROR: Can't get time")
        except Exception as e:
            print(" >", e)

    def shedule_handler(self):
        try:
            self.sync_time()
            self.set_door_status()
        except Exception as e:
            print(" >", e)

    def run_sheduler(self):
        try:
            print("### SHED_INFO: Trying to REinit shedule handler")
            self.shedule_handler_timer.interval(1000, self.shedule_handler)
            self.shedule_handler_timer.start()
        except Exception as e:
            print(" >", e)

    def start_sheduler(self):
        try:
            print("### SHED_INFO: Trying to init shedule handler")
            self.shedule_handler_timer=timers.timer()
            self.shedule_handler_timer.interval(1000, self.shedule_handler)
            self.shedule_handler_timer.start()
        except Exception as e:
            print(" >", e)

    def stop_sheduler(self):
        try:
            self.shedule_handler_timer.clear()
        except Exception as e:
            print(" >", e)