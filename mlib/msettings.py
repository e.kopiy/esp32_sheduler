import json
import flash
import gc

class Msettings():
    saddress = 0x00310000
    # endaddress = 0x40275BEA
    payload = []
    max_settings_sz = 8 * 1024
    settings_present_mark = 0x11EE11DD
    
    getip_port = 6800
    tcp_server_port = 6900
    
    # settings_present_mark = 0
    
    settings = {}
    
    def __init__(self):
        pass
    
    def settings_init(self):
        print("Initializing settings area...")
        try:
            addr = self.saddress
            ff = flash.FlashFileStream(addr, 512)
            ff.write(self.settings_present_mark)
            settings_sz = 0
            ff.write(settings_sz)
            #print("Call flush")
            ff.flush()
            #print("Call close")
            ff.close()
            self.settings = {}
        except Exception as e:
            print("Initializing exception:")
            print(e)
            
            
    def write_settings(self):
        print("Write settings")
        try:
            jspl = json.dumps(self.settings)
            slen = 8 + len(jspl)
            addr = self.saddress
            ff = flash.FlashFileStream(addr, self.max_settings_sz + 8 + 1)
            ff.write(self.settings_present_mark)
            
            #print("write_settings: settings length: ", len(jspl))
            ff.write(len(jspl))
            #print(jspl)
            
            ff.write(jspl)

            ff.flush()
            ff.close()
        except Exception as e:
            print("write_settings exception:")
            print(e)  

    def read_settings(self):
        print("Read settings")
        try:
            addr = self.saddress
            ff = flash.FlashFileStream(addr, 8 + 1)
            # print("read_settings: reading settings present mark")
            settings_pres = ff.read_int()
            if settings_pres != self.settings_present_mark:
                # print("Settings area are not initialized: " + str(settings_pres))
                self.settings_init()
                return {}
                
            # print("read_settings: reading settings size")
            settings_sz = ff.read_int()
            print("FreeMem1-4:", str(gc.info()[1]//1024))
            if settings_sz > self.max_settings_sz:
                # print("read_settings error: settings_sz > max_settings_sz")
                ff.close()
                return {}
            if settings_sz == 0:
                # print("read_settings: settings zero size")
                ff.close()
                return {}   
            ff.close()
            # print("read_settings: settings length: " + str(settings_sz))
            # print("read_settings: reading settings contents")            
                
            ff = flash.FlashFileStream(addr, settings_sz + 8 + 1)
            ss = ""
            for i in range(8, settings_sz + 8):
                ss = ss + chr(ff[i])
            #print("read_settings: Settings area contents:" )
            if ss != "":
                self.settings = json.loads(ss)
            ss = ""

            ff.close()

            #bb = bytearray(5)
            #ss = __read_flash(addr,5, bb)
            #__write_flash(self.addr,self.bb)
            return self.settings
            
        except Exception as e:
            print("read_settings exception:")
            print(e)  
            return {}            
        