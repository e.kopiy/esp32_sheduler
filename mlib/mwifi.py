from wireless import wifi
# from espressif.esp8266wifi import esp8266wifi as wifi_driver
from espressif.esp32net import esp32wifi as wifi_driver
import mcu
import json
import rtc
import timers

import socket

class Mwifi():
    mutils_inst = None
    ap_created = False
    
    now_connecting = False

    mcu_ip = ''
    
    conn_handler_timer = None
    ap_handler_timer = None
    
    reconnect_cnt = 0
    recreate_cnt = 0
    connected = False
    
    def __init__(self, mutils_inst):
        self.mutils_inst = mutils_inst

        if "wifi" not in self.mutils_inst.stgs.settings:
            self.mutils_inst.stgs.settings["wifi"] = {}

        self.drv_init()
        linfo = wifi.link_info()
        mac = ":".join([hex(x,"") for x in linfo[4]])
        print("Local iface MAC: " + mac)

    def switchMode():
        print("### INFO: Reboot button pressed. Rebooting.")
        mcu.reset()

    def ext_monit_blink(self):
        print("Start blinking")
        while True:
            try:
                if (self.connected == True):
                    cnt1 = 0
                    cnt2 = 0
                    digitalWrite(D4, HIGH)
                    while cnt1 <= 20:
                        sleep(5)
                        cnt1 += 1
                    digitalWrite(D4, LOW)
                    while cnt2 <= 200:
                        sleep(5)
                        cnt2 += 1
                else:
                    cnt1 = 0
                    cnt2 = 0
                    digitalWrite(D4, HIGH)
                    while cnt1 <= 100:
                        sleep(5)
                        cnt1 += 1
                    digitalWrite(D4, LOW)
                    while cnt2 <= 20:
                        sleep(5)
                        cnt2 += 1
            except Exception as e:
                print(e)
                raise e
    
    def run_ext_monit_blink(self):
        thread(self.ext_monit_blink, self)

    def drv_init(self):
        try:
            print("Init wifi driver...")
            wifi_driver.auto_init()
        except Exception as e:
            print("ooops, something wrong while init wifi driver :(", e)
        print("Wifi driver initialized")
    
    def get_sta_name_passw(self):
        stn = ""
        if "wifi" in self.mutils_inst.stgs.settings:
            if "stn" in self.mutils_inst.stgs.settings["wifi"]:
                if self.mutils_inst.stgs.settings["wifi"]["stn"] != "":
                    stn = self.mutils_inst.stgs.settings["wifi"]["stn"]
                
        stp = ""
        if "wifi" in self.mutils_inst.stgs.settings:
            if "app" in self.mutils_inst.stgs.settings["wifi"]:
                if self.mutils_inst.stgs.settings["wifi"]["stp"] != "":
                    stp = self.mutils_inst.stgs.settings["wifi"]["stp"]
                    
        return [stn, stp]

    def connect(self):
        print("Establishing wifi link...")

        self.now_connecting = True
        onPinFall(BTN0, None)

        i=0
        while i<10:
            digitalWrite(D4, HIGH)
            sleep(100)
            digitalWrite(D4, LOW)
            sleep(100)
            i += 1
        digitalWrite(D4, HIGH)
        
        ret = True
        try:
            stn, stp = self.get_sta_name_passw()

            if stn != "":
                wifi.link(stn, wifi.WIFI_WPA2, stp)
            else:
                print("STA access point name empty. Skip connecting:",self.mutils_inst.stgs.settings)
        except Exception as e:
            print("Can't connect to " + stn + ", ", e)
            ret = False
        
        if wifi.is_linked():
            linfo = wifi.link_info()
            print("Connected to: " + stn + ",connection info: " + json.dumps(linfo))
            self.mcu_ip = linfo[0]
            print("MCU ip: ", self.mcu_ip)
        else:
            print("Unit can't link to wifi AP")
            ret = False
        onPinFall(BTN0, self.switchMode)
        self.now_connecting = False
        return ret
    
    def net_config_is_ok(self):
        if wifi.is_linked() and wifi.link_info()[0] != "0.0.0.0" and wifi.link_info()[0] != "192.168.4.1":
            return True
        else:
            return False
    
    def disconnect(self):
        print("Terminating wifi link...")
        ret = True
        try:
            wifi.unlink()
        except Exception as e:
            print("Can't terminate wifi connection", e)
            ret = False

        return ret
        
    def get_ap_name_passw(self):
        apn = "DbgAP_" + self.mutils_inst.mcuuid[-6:]
        if "wifi" in self.mutils_inst.stgs.settings:
            if "apn" in self.mutils_inst.stgs.settings["wifi"]:
                if self.mutils_inst.stgs.settings["wifi"]["apn"] != "":
                    apn = self.mutils_inst.stgs.settings["wifi"]["apn"]
                
        app = "111222333444555"
        if "wifi" in self.mutils_inst.stgs.settings:
            if "app" in self.mutils_inst.stgs.settings["wifi"]:
                if self.mutils_inst.stgs.settings["wifi"]["app"] != "":
                    app = self.mutils_inst.stgs.settings["wifi"]["app"]

        return [apn, app]

    def setup_ap(self):
        print("Setup wifi AP")
        try:
            apn, app = self.get_ap_name_passw()

            wifi.softap_init(apn, 3, app)
            ainfo = wifi.softap_get_info()
            print("AP started, IP:", ainfo[0])
            self.ap_mode  = 1
        except Exception as e:
            print("Can't setup AP with ssid " + apn + ", ", e)
            return False

        print("Wifi AP is up, ssid: " + apn)

        return True
        
    def unsetup_ap(self):
        try:
            print("Unsetuping wifi AP")
            wifi.softap_off()
            print("Wifi AP unsetuped:")
            self.ap_mode  = -1
        except Exception as e:
            print("Can't unsetup AP,", e)
            return False
        return True

    def conn_handler(self):
        if wifi.is_linked() and self.net_config_is_ok():
            self.connected = True
            print("### CONN_HANDLER: WIFI is linked and net_cfg OK!")
            self.reconnect_cnt = 0
        else:
            self.connected = False
            # if no link for 8 hours then rebooting
            if (self.reconnect_cnt > 5760):
                print("### WIFI_ERROR: No successful connections for eight hours. Rebooting MCU")
                mcu.reset()
            else:
                print("### WIFI_WARNING: WiFi link check error. Trying to reconnect.")
                self.connect()
                self.reconnect_cnt += 1

    def ap_handler(self):
        if self.ap_created:
            self.recreate_cnt = 0
        else:
            if self.recreate_cnt > 3:
                print("### WIFI_ERROR: Can't create AP for 3 times. Rebooting MCU")
                mcu.reset()
            else:
                print("### WIFI_WARNING: AP status error. Tryig to reinit AP.")
                self.ap_created = self.setup_ap()
                self.recreate_cnt += 1
    
    def stop_conn_handler(self):
        print("### SHED_INFO: Trying to stop conn_handler")
        self.conn_handler_timer.clear()

    def stop_ap_handler(self):
        print("### SHED_INFO: Trying to stop ap_handler")
        self.ap_handler_timer.clear()

    def run_conn_handler(self):
        print("### SHED_INFO: Trying to REinit conn_handler")
        self.conn_handler_timer.interval(5000, self.conn_handler)
        self.conn_handler_timer.start()

    def run_ap_handler(self):
        print("### SHED_INFO: Trying to REinit ap_handler")
        self.ap_handler_timer.interval(5000, self.ap_handler)
        self.ap_handler_timer.start()
    
    def start_conn_handler(self):
        print("### SHED_INFO: Trying to init conn_handler")
        self.conn_handler_timer=timers.timer()
        self.conn_handler_timer.interval(5000, self.conn_handler)
        self.conn_handler_timer.start()

    def start_ap_handler(self):
        print("### SHED_INFO: Trying to init timesync handler")
        self.ap_handler_timer = timers.timer()
        self.ap_handler_timer.interval(5000, self.ap_handler)
        self.ap_handler_timer.start()