# Created at 2019-02-19 08:07:55.144570

import streams
import gc
import mcu
import pwm
import timers

from mlib import mutils
from mlib import mwifi
from mlib import msettings
from mlib import msockserver

# import the wifi interface
from wireless import wifi

# the wifi module needs a networking driver to be loaded
# in order to control the board hardware.
# This example can be used as is with ESP32 based devices
from espressif.esp32net import esp32wifi as wifi_driver

# import Real-Time Clock module
import rtc

pinMode(BTN0, INPUT_PULLUP)
            
def check_ap_state():
    ret = False
    try:
        print("Check AP mode required")
        pinMode(D4, OUTPUT)
        cpin = LED0
        pinMode(cpin, OUTPUT)
        digitalWrite(cpin, HIGH)
        digitalWrite(D4, HIGH)
        cnt = 0
        cnt2 = 0
        
        while cnt<500:
            if digitalRead(BTN0) == 0:
                ret = True
                while cnt2<5:
                    digitalWrite(D4, LOW)
                    sleep(150)
                    digitalWrite(D4, HIGH)
                    sleep(150)
                    cnt2 += 1
                break
            cnt+=1
            sleep(10)
            
        digitalWrite(cpin, LOW)
        digitalWrite(D4, LOW)
    
    except Exception as e:
        print(e)
        raise e
    
    return ret
    
def switchMode():
    print("### INFO: Reboot button pressed. Rebooting.")
    mcu.reset()

try:
    pinMode(D5,OUTPUT)
    digitalWrite(D5, HIGH)
    #streams.serial(SERIAL2,baud=115200,set_default=True)
    streams.serial()
    ap_state = check_ap_state()
    if ap_state == True:
        digitalWrite(D4, HIGH)

    onPinFall(BTN0, switchMode)

    print("AP state: ", ap_state)
    
    print("FreeMem0:", str(gc.info()[1]//1024))

    mutils_inst = mutils.MUtils(ap_state)
    print("Starting device with uid: ", mutils_inst.mcuuid)
    
    mutils_inst.run_utils()
    if ap_state == False:
        mutils_inst.mwifi_inst.run_ext_monit_blink()
    
except Exception as e:
    print(e)
